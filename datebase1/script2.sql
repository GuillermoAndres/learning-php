-- Recordar todo el conocimiento pasado
-- Ingresamos con nuestro usuario
mysql -u jim -p
-- Listamos nuestra bases de datos;
SHOW DATABASES;
-- Seleccionamos con la base que vamos a trabajar
USE publications;
-- Listamos todas nuestras tablas de nuestra base de datos publications.
SHOW FULL TABLES FROM publications;
-- Borrados otra vez nuestra tabla para practicar nuevamente
DROP TABLE IF EXISTS classics;

-- Creamos nuevamente nuestra tabla.
CREATE TABLE classics (
author VARCHAR(128),
title VARCHAR(128),
type VARCHAR(16),
year CHAR(4),
id INT UNSIGNED NOT NULL AUTO_INCREMENT KEY) ENGINE InnoDB;

-- Ver estructura de la tabla
DESCRIBE classics;

-- Eliminar id columna de la tabla
ALTER TABLE classics DROP id;

-- Insertar valores de la tabla classics;
INSERT INTO classics(author, title, type, year)
 VALUES('Mark Twain','The Adventures of Tom Sawyer','Fiction','1876');
INSERT INTO classics(author, title, type, year)
 VALUES('Jane Austen','Pride and Prejudice','Fiction','1811');
INSERT INTO classics(author, title, type, year)
 VALUES('Charles Darwin','The Origin of Species','Non-Fiction','1856');
INSERT INTO classics(author, title, type, year)
 VALUES('Charles Dickens','The Old Curiosity Shop','Fiction','1841');
INSERT INTO classics(author, title, type, year)
 VALUES('William Shakespeare','Romeo and Juliet','Play','1594');

SELECT * FROM classics;
-- SMALLINT - 32767 (2 bytes)
-- SMALLINT UNSIGNED - 65,535 values
