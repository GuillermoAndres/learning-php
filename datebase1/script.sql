
SHOW database;

-- Cancelling command
-- \c
-- Exit
-- \q 

-- Create a new database
CREATE DATABASE publications;

-- Selccionar database 
USE publications;

-- Crear usuario quien solo podra usar nuestra base de datos publications
CREATE USER 'jim'@'localhost' IDENTIFIED BY '1234';
-- Dado que la constraseña es demasiada insegura no te dejara ingresar
-- Ver la politica de las contraseñas en Mysql
SHOW VARIABLES LIKE 'validate_password%';
-- Cambiar politicas
SET GLOBAL validate_password.LENGTH = 4;
SET GLOBAL validate_password.policy = LOW;
SET GLOBAL validate_password.mixed_case_count = 0;
SET GLOBAL validate_password.number_count = 0;
SET GLOBAL validate_password.special_char_count = 0;
SET GLOBAL validate_password.check_user_name = 0;

-- https://dev.mysql.com/doc/refman/5.6/en/validate-password-options-variables.html
-- https://stackoverflow.com/questions/43094726/your-password-does-not-satisfy-the-current-policy-requirements

-- Otorgar todos los permisos, parecido al usuario root, al esquema(database)
-- publications, a las demas bases no tendra acceso.
GRANT ALL PRIVILEGES ON publications.* TO 'jim'@'localhost';

-- Refrescar cambios
FLUSH PRIVILEGES;

-- https://www.digitalocean.com/community/tutorials/crear-un-nuevo-usuario-y-otorgarle-permisos-en-mysql-es

-- Para ver los usuarios que hay en el sistema, se debe logear con root tiene
-- los permisos para ver de la tabla mysql.user
SELECT User, Host, Password FROM mysql.user;

-- Logearte ahora con el usuario jim 
/* > mysql -u jim -p */ 
/* Ingresar password: **** */

-- Ver usuario actual
SELECT user();

-- https://www.rosehosting.com/blog/mysql-show-users/


-- Una vez ingresado, debemos de indicar que esquema o databse utilizaremos,
-- asi sabra donde colocar la nuevas tablas que crearemos.
USE publications;

-- Creatin table
CREATE TABLE classics (
    author VARCHAR(128),
    title VARCHAR(128),
    type VARCHAR(16),
    year CHAR(4)
);

-- Ver estructura de la tabla
DESCRIBE classics;
DESC classics;

-- Agregamos un id_classics 
ALTER TABLE classics ADD id_classics INT UNSIGNED NOT NULL AUTO_INCREMENT KEY;

-- ALTER
-- Add, change and delete columns

-- Revome id_classics atribute
ALTER TABLE classics DROP id_classics;

-- Insertando valores 
-- INSERT INTO tabla(nameAtributes,...,) VALUES ('Values');

INSERT INTO classics(author, title, type, year)
VALUES('Mark Twain','The Adventures of Tom Sawyer','Fiction','1876');
INSERT INTO classics(author, title, type, year)
VALUES('Jane Austen','Pride and Prejudice','Fiction','1811');
INSERT INTO classics(author, title, type, year)
VALUES('Charles Darwin','The Origin of Species','Non-Fiction','1856');
INSERT INTO classics(author, title, type, year)
VALUES('Charles Dickens','The Old Curiosity Shop','Fiction','1841');
INSERT INTO classics(author, title, type, year)
VALUES('William Shakespeare','Romeo and Juliet','Play','1594');


-- Ver contenido de las tablas
SELECT * FROM classics;

-- Change name table
ALTER TABLE classics RENAME clasicos;
ALTER TABLE clasicos RENAME classics;





