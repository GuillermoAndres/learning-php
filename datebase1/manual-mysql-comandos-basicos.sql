
-- Ver database
SHOW DATABASES;
-- Ver tablas
SHOW TABLES;
-- Ver usuario
SELECT user()
-- Usar database 
USE publications;

----


-- Crear tabla 
CREATA TABLE alumnos (
nombre VARCHAR(45),
)
-- Rename table
ALTER TABLE classics RENAME clasicos;
-- Delete table
DROP TABLE classics;
-- Insert values
INSERT INTO classics(author, title, type, year) VALUES('Mark Twain','The Adventures of Tom Sawyer','Fiction','1876');


-----------------
-- Add column
ALTER TABLE classics ADD pages SMALLINT UNSIGNED;
-- Delete column
ALTER TABLE classics DROP pages;
-- Rename name column
ALTER TABLE classics CHANGE type category VARCHAR(16);
-- Rename type column 
ALTER TABLE classics MODIFY year SMALLINT;
-- --------------------------------------------


