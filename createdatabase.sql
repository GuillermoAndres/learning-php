-- Antes que nada, levantamos nuestro MySQL database server con:
-- sudo systemctl start mysqld

-- Ingresamos sesion:
-- mysql -u root -p

-- Para empezar primero crearemos una database donde se guardar las tablas,
-- habra miles de database en mysql y cada una contendra un conjunto de tablas.
-- Es la forma de organizar nuestra tablas.

CREATE DATABASE aliendatabase;

-- Una vez creada debemos seleccionarla para indicar que ahi se guardaran
-- nuestras tablas.
USE aliendatabse;

-- Una vez cambiada a la base que usaremos, empezamos a crear tablas
CREATE TABLE aliens_abduction (
    first_name varchar(30),
    last_name varchar(30),
    when_it_happened varchar(30),
    how_long varchar(30),
    how_many varchar(30),
    alien_description varchar(100),
    what_they_did varchar(100),
    fang_spotted varchar(10),
    other varchar(100),
    email varchar(50)
);

-- Una vez creada nuestras tablas, corresponde a insertar registros

INSERT INTO aliens_abduction VALUES ('Alf', 'Nader', '2000-07-12', 'one week', 'at least 12', 'It was a big non-recyclable shiny disc full of what appeared to be mutated labor union officials.', 'Swooped down from the sky and snatched me up with no warning.', 'no', 'That\'s it.', 'alf@nader.com');
INSERT INTO aliens_abduction VALUES ('Don', 'Quayle', '1991-09-14', '37 seconds', 'dunno', 'They looked like donkeys made out of metal with some kind of jet packs attached to them.', 'I was sitting there eating a baked potatoe when "Zwoosh!", this beam of light took me away.', 'yes', 'I really do love potatos.', 'dq@iwasvicepresident.com');
INSERT INTO aliens_abduction VALUES ('Rick', 'Nixon', '1969-01-21', 'nearly 4 years', 'just one', 'They were pasty and pandering, and not very forgiving.', 'Impeached me, of course, then they probed me.', 'no', 'I\'m lonely.', 'rnixon@not');
INSERT INTO aliens_abduction VALUES ('Belita', 'Chevy', '2008-06-21', 'almost a week', '27', 'Clumsy little buggers, had no rhythm.', 'Tried to get me to play bad music.', 'no', 'Looking forward to playing some Guitar Wars now that I\'m back.', 'belitac@rockin.net');
INSERT INTO aliens_abduction VALUES ('Sally', 'Jones', '2008-05-11', '1 day', 'four', 'green with six tentacles', 'We just talked and played with a dog', 'yes', 'I may have seen your dog. Contact me.', 'sally@gregs-list.net');
INSERT INTO aliens_abduction VALUES ('Meinhold', 'Ressner', '2008-08-10', '3 hours', 'couldn\'t tell', 'They were in a ship the size of a full moon.', 'Carried me to the top of a mountain and dropped me off.', 'no', 'Just want to thank those fellas for helping me out.', 'meinhold@icanclimbit.com');
INSERT INTO aliens_abduction VALUES ('Mickey', 'Mikens', '2008-07-11', '45 minutes', 'hundreds', 'Huge heads, skinny arms and legs', 'Read my mind,', 'yes', 'I\'m thinking about designing a helmet to thwart future abductions.', 'mickey@stopreadingmymind.net');
INSERT INTO aliens_abduction VALUES ('Shill', 'Watner', '2008-07-05', '2 hours', 'don\'t know', 'There was a bright light in the sky, followed by a bark or two.', 'They beamed me toward a gas station in the desert.', 'yes', 'I was out of gas, so it was a pretty good abduction.', 'shillwatner@imightbecaptkirk.com');


