Existen tres formas de conectarte a la base de datos (Es decir, mandar
peticiones al database server).

1- Via linea de comandos
2- PhpMyAdmin (Interfaz web) o Mysql-Workbench (Aplicacion escritorio)
3- Lenguaje de programacion

Instalacion PhpMyAdmin 
> sudo yum install phpMyAdmin

Hay otra herramiente que ofrece la pagina oficial llamda MySQL Workbench
Esta se instala desde su pagina oficil y se descarga el rpm
(mysql-workbench-community-8.0.25-1.fc34.x86_64.rpm)

Para instalar workbench necesitaras unas dependencias
sudo rpm -i mysql-workbench-community-8.0.25-1.fc34.x86_64.rpm
Con rpm no maneja las dependecias y tendras que descarlas por tu cuenta:
https://fedora.pkgs.org/34/fedora-aarch64/libglvnd-opengl-1.3.2-3.fc34.aarch64.rpm.html
https://fedora.pkgs.org/34/fedora-x86_64/pcre-cpp-8.44-3.fc34.1.x86_64.rpm.html
https://fedora.pkgs.org/34/fedora-x86_64/proj-7.2.1-2.fc34.x86_64.rpm.html

Si lo instalas con yum si maneja las independencias y te las descarga:
sudo yum install mysql-workbench-community-8.0.25-1.fc34.x86_64.rpm

# References
https://fedoraproject.org/wiki/PhpMyAdmin
https://dev.mysql.com/downloads/workbench/
https://docs.fedoraproject.org/en-US/quick-docs/installing-mysql-mariadb/


https://wiki.archlinux.org/title/MySQL_(Espa%C3%B1ol)
https://es.wikipedia.org/wiki/MySQL_Workbench
https://es.wikipedia.org/wiki/PhpMyAdmin
https://fedoraproject.org/wiki/MariaDB

https://stackoverflow.com/questions/13876875/how-to-make-rpm-auto-install-dependencies
