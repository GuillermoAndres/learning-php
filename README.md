# Levantar servicio
Primeros verificamos si el servicio esta activo con el comando:

![Comando systemctl](./img/verificarServicio.png)

El servcio de apache en Fedora es llamado httpd, en Ubunto apache2:

![httpd](./img/serviciohttpd.png)

Comando para levantar servicio
~~~
systemctl start httpd.service
~~~
![start](img/status.png)

Direccion para ingresar es:
~~~
localhost:80
http://localhost:80
localhost
~~~
(Como el puerto 80 es por defecto para un servicio web, por eso no es 
necesario ponerlo, en caso de MacOS me parece que utiliza el puerto 8080)

![home](img/home.png)

La carpeta raiz sera:
~~~
/var/www/html
~~~
Ahi se alojaran todos los archivos html,css,...etc, de nuestro sitio web.

Un tick podria es crear un enlace directo de /var/www/html para no ingresar
cada vez a su carpeta raiz y no sea tedioso.

# Stop el servicio
Para para el servicio, ejecute:
~~~
systemctl stop httpd.service
~~~

# Usando solo php
Si instalas php, puedes crear un servicio y un puerto en una carpeta particular
para trabarjar, esto se hace con el comando:
~~~
php -S localhost:4000
~~~
Esa sera tu raiz del proyecto en la carpeta donde lo ejecutes.


# Levantar MySQL Server
Primero tenemos que levantar el servicio para poder ocupar nuestro 
manejador de bases de datos, ya que lo que hacemo nosotros es mandar
una peticion al manejador (DBMS) para que despues nos devuelva una
respuesta, en este caso, sera la consulta que nosotros pedimos.
Si no se levanta el servicio, se genera este error:
~~~
ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' (2)
~~~

Comando para levantarlo:
~~~
sudo systemctl start mysqld
~~~

![levando mysql](img/levandoMysql.png)


Si no quieres levantar el servicio cada vez que lo ocupes, lo que puedes
hacer es habilitarlo para que corra cuando se enciende tu computadora, esto
puede hacerte con cualquier servicio.
~~~
sudo systemctl enable mysqld
~~~

There are three main ways you can interact with MySQL: using a command line, via a
web interface such as phpMyAdmin, and through a programming language like PHP.


