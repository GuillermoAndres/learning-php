<html>
  <head>
    <title> Aliens Abducted Me - Report an Abduction</title>
  </head>
  <body>
    <h2>Aliens Abducted Me - Report an Abduction</h2>
    <?php
    $name = $_POST['firstname'] . ' ' . $_POST['lastname'];
    $email = $_POST['email'];
    $when_it_happened = $_POST['whenithappend'];
    $how_long = $_POST['howlong'];
    $how_many = $_POST['howmany'];
    $alien_description = $_POST['aliendescription'];
    $what_they_did = $_POST['whattheydid'];
    $fang_spotted = $_POST['fangspotted'];
    $other = $_POST['other'];
    
    
    /* Since Owen’s sending email messages as plain text with no HTML
     * formatting, he can’t just stick in <br /> tags to add line breaks where the
     * content’s running together. But he can use newline characters, which are
     * escaped as \n. So wherever \n appears in the email text, a newline will
     * be inserted, causing any content after it to start on the next line. 
     * Escape characters in PHP start with a backslash (\) */

    $msg = $name . ' was abducted ' . $when_it_happened . ' and was gone for ' . $how_long . '.\n' .
           'Number of aliens: ' . $how_many . '\n' .
           'Alien description: ' . $alien_description . '\n' .
           'What they did: ' . $what_they_did . '\n' .
           'Fang spotted: ' . $fang_spotted . '\n' .
           'Other comments: ' . $other;

    /* The problem with Owen’s code is that PHP handles strings differently
     * depending on whether they’re enclosed by single or double quotes. More
     * specifically, newline characters (\n) can only be escaped in double-quoted
     * strings. */

    /* Single-quoted strings are considered raw text, whereas PHP processes double-quoted
     * strings looking for variables. When a variable is encountered within a
     * double‑quoted string, PHP inserts its value into the string as if the strings
     * had been concatenated. So not only is a double-quoted string necessary to
     * make the newlines work in the email message, but it also allows us to simplify
     * the code by sticking the variables directly in the string. */

    $msg = "$name was abducted $when_it_happened and was gone for $how_long. \n" .
           "Number of aliens: $how_many\n" .
           "Alien description: $alien_description\n" .
           "What they did: $what_they_did \n" .
           "Fang spotted: $fang_spotted \n" .
           "Other comments: $other";   
    
    //echo $msg;          

    $to = 'memocampeon35@gmail.com';
    $subject = 'Aliens Abducted Me Report';
    mail($to, $subject, $msg, 'From: ' . $email);
    
    echo 'Thaks for submitting the form. <br> ';
    echo 'You were abducted: ' . $when_it_happened . '<br>';
    echo 'and were gone abducted: ' . $how_long . '<br>';
    echo 'Describe them: ' . $alien_description . '<br>';
    echo 'Was Fang there: ' . $fang_spotted . '<br>';
    echo 'Your email address is: ' . $email . '<br>';

    ?>
  </body>
</html>
